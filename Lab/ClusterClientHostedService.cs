using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Orleans;
using Orleans.Configuration;
using Orleans.Runtime;

namespace OrleansClient
{
    public static class BuilderExt
    {
        public static IClientBuilder ConfigureCluster(this IClientBuilder builder, string mongoDbEnv = "mongoDB")
        {
            var dbName = Environment.GetEnvironmentVariable(mongoDbEnv);
            if (dbName == null) {
                builder.UseLocalhostClustering();
            } else {
                var mongoAddr  = Environment.GetEnvironmentVariable("mongoConnect") ?? "mongodb://localhost:27017";
                
                System.Console.WriteLine($"Clustering in Mongo Db: {dbName} at {mongoAddr}");
                builder
                    .UseMongoDBClient(mongoAddr)
                    .UseMongoDBClustering(options => { options.DatabaseName = dbName; });
            }

            return builder.Configure<ClusterOptions>(options => {
                options.ClusterId = "dev";
                options.ServiceId = "HelloWorldApp";
            });
        }
    }
    public class ClusterClientHostedService : IHostedService
    {
        private readonly ILogger<ClusterClientHostedService> _logger;

        public static IClusterClient Client;

        public ClusterClientHostedService(ILogger<ClusterClientHostedService> logger, ILoggerProvider loggerProvider)
        {
            _logger = logger;
            Client = new ClientBuilder()
                .ConfigureCluster()
                .ConfigureLogging(builder => builder.AddProvider(loggerProvider))
                .Build();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            var attempt = 0;
            var maxAttempts = 100;
            var delay = TimeSpan.FromSeconds(1);
            return Client.Connect(async error =>
            {
                if (cancellationToken.IsCancellationRequested)
                {
                    return false;
                }

                if (++attempt < maxAttempts)
                {
                    _logger.LogWarning(error,
                        "Failed to connect to Orleans cluster on attempt {@Attempt} of {@MaxAttempts}.",
                        attempt, maxAttempts);

                    try
                    {
                        await Task.Delay(delay, cancellationToken);
                    }
                    catch (OperationCanceledException)
                    {
                        return false;
                    }

                    return true;
                }
                else
                {
                    _logger.LogError(error,
                        "Failed to connect to Orleans cluster on attempt {@Attempt} of {@MaxAttempts}.",
                        attempt, maxAttempts);

                    return false;
                }
            });
        }

        public async Task StopAsync(CancellationToken cancellationToken)
        {
            try
            {
                await Client.Close();
            }
            catch (OrleansException error)
            {
                _logger.LogWarning(error, "Error while gracefully disconnecting from Orleans cluster. Will ignore and continue to shutdown.");
            }
        }

        //public IClusterClient Client { get; }
    }
}
