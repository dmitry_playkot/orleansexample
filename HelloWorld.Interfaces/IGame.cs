using System.Threading.Tasks;
using Orleans;
using SharedLogic;

namespace HelloWorld.Interfaces
{
    public interface IGame : Orleans.IGrainWithIntegerKey
    {
        Task Setup(string player1, string player2);
        Task<MoveResponse> ProcessMove(MoveRequest move);

        Task<MoveResponse> Check4Update(int turn);
    }
    
    public static partial class Ext
    {
        public static IGame Game(this IGrainFactory factory, int gameId) => factory.GetGrain<IGame>(gameId);
    }
}