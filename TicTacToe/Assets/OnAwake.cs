﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnAwake : MonoBehaviour
{
    public enum Action
    {
        NoAction,
        ActiveInEditor,
        InactiveInEditor
    }

    [SerializeField] private Action _action;
    private void Awake()
    {
        var inEditor = false;
#if UNITY_EDITOR
        inEditor = true;
#endif
        switch (_action)
        {
            case Action.ActiveInEditor:
                gameObject.SetActive(inEditor);
                break;
            case Action.InactiveInEditor:
                gameObject.SetActive(!inEditor);
                break;
        }
    }
}
